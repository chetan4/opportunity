# Opportunity

Create lists and send emails by connecting your gmail account


## System requirements
  - Ruby version 2.4.x
  - Mysql

## Installation
  - Ruby installtion
    perhaps the best source out there that describe about Ruby installtion is [RVM](https://rvm.io/)
    after rvm is installed once can installed the ruby version 2.4.x series by running

    `rvm install ruby-2.4.x`

    Some useful RVM  commands
      - rvm list (list all the ruby versions installed via rvm)
      - rvm use ruby-2.4.x (will set the ruby version as 2.4.x)
      - rvm use ruby-2.4.x  --default (will set this ruby version as default ruby version)


    Alternately,

    One can also installed Ruby purely via [Ruby Source](https://www.ruby-lang.org/en/downloads/).
      * [Installation](https://www.ruby-lang.org/en/documentation/installation/)
      * [Mirror](https://cache.ruby-lang.org/pub/ruby/2.4/)

  - Mysl Installation
    * [MacOS](https://dev.mysql.com/downloads/mysql/)
    * [Ubuntu](https://dev.mysql.com/downloads/mysql/)

## Getting Started

  - Clone the REPO
    * [Idyllic Software](https://github.com/idyllicsoftware/opportunity)

  - Setup
    Inside the Views directory, *run* the following command(only once)
      * [BUNDLE](http://bundler.io/)
        - `bundle install`

      * If you don't have Bundle, installed it like.
        - ` gem install bundler `

    and repeat the same step i.e `bundle install` and move on.

  - Create database.yml from database.yml.sample file, update database configuration(username and password).
  - Run the following command.
    * `rake db:create`
    * `rake db:migrate`
    * `rake db:seed`

